# Twitter Sentiment Analysis

In this project, a random forest classifier predicts hate speech on Twitter.

The .yml environment file defines which libraries have been used, it can be directly imported into anaconda. Note that Twitter policy prevents me from uploading some datasets and personal access tokens of Twitter API.
